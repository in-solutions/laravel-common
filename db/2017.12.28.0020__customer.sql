SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `t_customer` (
`id` bigint(20) unsigned NOT NULL,
  `company_id` bigint(20) unsigned DEFAULT NULL,
  `person_id` bigint(20) unsigned NOT NULL,
  `invoice_address_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `t_customer`
 ADD PRIMARY KEY (`id`), ADD KEY `invoice_address_id` (`invoice_address_id`), ADD KEY `company_id` (`company_id`), ADD KEY `person_id` (`person_id`);


ALTER TABLE `t_customer`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `t_customer`
ADD CONSTRAINT `t_customer_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `t_company` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `t_customer_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `t_person` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `t_customer_ibfk_3` FOREIGN KEY (`invoice_address_id`) REFERENCES `t_address` (`id`) ON UPDATE CASCADE;
