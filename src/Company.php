<?php

namespace Insolutions\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use SoftDeletes;

    protected $table = 't_company';

    protected $hidden = ['deleted_at','created_at','updated_at','address_id'];
    
    protected $fillable = ['name'];

    public function address() {
    	return $this->belongsTo(Address::class);
    }
}