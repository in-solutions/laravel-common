<?php

namespace Insolutions\Common;

use Illuminate\Support\Facades\DB;

use Insolutions\Common\Person;
use Insolutions\Common\Company;
use Insolutions\Common\Address;
use Insolutions\I18n\Country;

class Service {

	public static function createCustomer($data): Customer {
		DB::beginTransaction();

    	try {
			// create
			$customer = new Customer;
	    	
    		$person = isset($data['person']['id'])
    			? Person::findOrFail($data['person']['id']) // load if ID is given
	    		: Person::create($data['person']);

	    	$customer->person()->associate($person);	    	

	    	if (isset($data['company'])) {
	    		// company will be present	
	    		$company = isset($data['company']['id'])
	    			? Company::findOrFail($data['company']['id']) // load if ID is given
		    		: Company::create($data['company']);

		    	$customer->company()->associate($company);
	    	}

	    	if (isset($data['invoice_address']['id'])) {
    			 // load if ID is given
    			$address = Address::findOrFail($data['invoice_address']['id']);
    		} else {
    			// country definition
    			$country_input = $data['invoice_address']['country'];
    			if (isset($country_input['id'])) {
    				$country = Country::findOrFail($country_input['id']);	
    			} else if ($country_input['iso3_code']) {
    				// search for country by iso3_code given
    				$country = Country::whereIso3Code($country_input['iso3_code'])->firstOrFail();
    			}
    			if (!$country) {
    				abort(404, "Country nod found by 'id' or 'iso3_code'");
    			}
    			

    			// create new address
	    		$address = new Address;
	    		$address->country()->associate($country);
	    		$address->fill($data['invoice_address'])->save();
    		}

	    	$customer->invoiceAddress()->associate($address);
	 	
	    	$customer->save();

	    } catch (Exception $e) {
	    	DB::rollBack();
	    	throw $e;
	    }

	    DB::commit();
	    return $customer;
	}
}