<?php

Route::group(['prefix' => 'common'], function () {
	
	Route::get('customer', 'Insolutions\Common\Controller@getCustomers');

	Route::post('customers', 'Insolutions\Common\Controller@create');
	Route::put('customers/{customer_id}', 'Insolutions\Common\Controller@update');
	Route::delete('customers/{customer_id}', 'Insolutions\Common\Controller@destroy');

});